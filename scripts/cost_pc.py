# Copyright (C) 2017  Balakumar Sundaralingam, Tucker Hermans, University of Utah

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Here the final cost is the desired fingertip positions and the running cost is the desired fingertip positions at each time step.
import numpy as np
from numpy.linalg import inv
import PyKDL



def cost(x_des,u,dyn_model,t=-1):
    wr=dyn_model.wr
    # cost for object pose:
    l_t=0.0

    des_contacts=dyn_model.time_contacts[int(t)]
    for i in dyn_model.fingers:
        des_pose=np.ravel(des_contacts[i][0:3,3])-np.ravel(dyn_model.end_effector_pose(u[i*4:i*4+4],i)[0:3,3])
        l_t+=np.dot(des_pose,des_pose)

    l_t=wr*l_t
    return l_t


def cost_gradient(x_des,u,dyn_model,t=-1):
    wr=dyn_model.wr

    gradient_fk=np.zeros(dyn_model.m)
    des_contacts=dyn_model.time_contacts[int(t)]

    for i in dyn_model.fingers:
        des_pose=np.ravel(des_contacts[i][0:3,3])-np.ravel(dyn_model.end_effector_pose(u[i*4:i*4+4],i)[0:3,3])
        gradient_fk[i*4:i*4+4]=-wr*des_pose*dyn_model.jacobian(u[i*4:i*4+4],i)

    return gradient_fk

def final_cost(x_des,u,dyn_model): 
    wp=dyn_model.wp
    # cost for object pose:
    l_obj=0.0
    for i in dyn_model.fingers:
        des_pose=np.ravel(dyn_model.des_contact_frames[i][0:3,3])-np.ravel(dyn_model.end_effector_pose(u[i*4:i*4+4],i)[0:3,3])
        l_obj+=np.dot(des_pose,des_pose)
    l=l_obj*wp
    return l


def final_cost_gradient(x_des,u,dyn_model):
    wp=dyn_model.wp
    gradient_fk=np.zeros(dyn_model.m)
    for i in dyn_model.fingers:
        des_pose=np.ravel(dyn_model.des_contact_frames[i][0:3,3])-np.ravel(dyn_model.end_effector_pose(u[i*4:i*4+4],i)[0:3,3])
        gradient_fk[i*4:i*4+4]=-wp*des_pose*dyn_model.jacobian(u[i*4:i*4+4],i)



    gradient=gradient_fk
    return gradient

