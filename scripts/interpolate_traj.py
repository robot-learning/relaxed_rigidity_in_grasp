# Copyright (C) 2017  Balakumar Sundaralingam, Tucker Hermans, University of Utah

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This file reads a trajectory file and interpolates the joint angles to make the trajectory slow and fit a larger time span.
import numpy as np

def interpolate():
    file=open('traj','r')
    pos_array=np.loadtxt(file)
    t=10
    T=100
    dt=10
    new_traj=np.zeros((T,16))
    
    for i in xrange(len(pos_array)-1):       
        print i
        print str(i*dt)+":"+str(i*dt+dt)
        for k in range(len(pos_array[i])):

            new_traj[i*dt:i*dt+dt,k]=np.linspace(pos_array[i,k],pos_array[i+1,k],dt)

    f=open("interp_test","w")
    for i in range(0,T):
        for k in range(16):
            f.write(str(new_traj[i,k]))
            f.write(' ')
        f.write('\n')
    
    f.close()

def interpolate_joints(pos_array,f_name):
    t=len(pos_array)-1
    T=100
    dt=T/t
    new_traj=np.zeros((T,16))



    for k in range(len(pos_array[0])):
        new_traj[0:dt,k]=np.linspace(pos_array[0,k],pos_array[1,k],dt)

    for i in range(1,len(pos_array)-1):
        print str(i)+' '+str(i+1)
        for k in range(len(pos_array[i])):
            new_traj[i*dt-1:i*dt+dt,k]=np.linspace(pos_array[i,k],pos_array[i+1,k],dt+1)


    f=open(f_name,"w")
    for i in range(0,T):
        for k in range(16):
            f.write(str(new_traj[i,k]))
            f.write(' ')
        f.write('\n')
    
    f.close()

def interpolate_traj(pos_array,T):
    t=len(pos_array)-1
    dt=T/t
    new_traj=np.zeros((T,16))

    for i in range(len(pos_array)-1):
        for k in range(len(pos_array[i])):
            new_traj[i*dt:i*dt+dt,k]=np.linspace(pos_array[i,k],pos_array[i+1,k],dt)
    return new_traj

