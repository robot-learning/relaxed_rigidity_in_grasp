# Copyright (C) 2017  Balakumar Sundaralingam, Tucker Hermans, University of Utah

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Runs the trajectory on the robot
import robot_node as robot
from object_list import *
from subprocess import Popen
import subprocess
import os
import signal
import time
n_trials=1
MAKE_TRIAL_LIST=False
#MAKE_TRIAL_LIST=True
ARUCO_=False
RECORD_BAG_=False

#EXP_NAME='exp_1_waypoint'
def read_objects(file,folder):
    obj_list=TrajList(file,folder)
    return obj_list


if __name__=='__main__':

    trial_name='relaxed-rigidity.csv'
    f_name='../data/trajectories/reachable_poses/'+method+'.csv'
    folder='../data/trials/'
    traj_folder='../data/trajectories/reachable_poses/'+method+'/'
    
    bag_folder='../bag/'+method+'/'

    if(MAKE_TRIAL_LIST):
        ''' RUN only once'''
        # read file and store as object list
        obj_=TrajList(f_name,folder)
        # Make trial list from object list
        obj_.write_trial_list(n_trials,trial_name)
        exit()
   
    allegroStates=robot.robotStates(listen_aruco=ARUCO_)   
    #bag_process.terminate()
    # Read trial list
    objects=TrajList(f_name,folder)
    #objects.add_aruco_centroid_locations('../data/object_grasps/object_centroids.csv')

    repeat=1
    t_no=-1
    trial_no=-1
    while(repeat<2):
        if(repeat==1):
            t_no_input=raw_input('Trajectory number ('+str(t_no+1)+'): ')
            if(t_no_input!=''):
                t_no=int(t_no_input)
            else:
                t_no+=1
            # Get Object
            print 'Object: '+objects.arr[t_no*n_trials].name
            trial_no=-1
        trial_no_input=raw_input('Trial number('+str(trial_no+1)+'): ')
        if(trial_no_input!=''):
            trial_no=int(trial_no_input)
        else:
            trial_no+=1
        index=t_no*n_trials+trial_no

        # Publishing joint positions to robot
        allegroStates.position_mode()
        allegroStates.publish_grasp(objects.arr[index].grasp)
    
        if(ARUCO_):

            P_=[] # list of subprocesses
            P_.append(allegroStates.tf_pose_pub_process(objects.arr[index].initial_pose,'i') )
            P_.append(allegroStates.tf_pose_pub_process(objects.arr[index].offset,'rigid','thumb_tip'))
            P_.append(allegroStates.tf_pose_pub_process(objects.arr[index].des,'G'))
            
            P_.append(allegroStates.tf_pose_pub_process(objects.arr[index].aruco,'obj_ar','ar') )

            raw_input('Placed Object and Aligned initial frames?')
            allegroStates.loop_rate.sleep()
            # Setup initial frame
            initial_frame=allegroStates.tf_find_transform('/i','/obj_ar')
            #palm_rotation=allegroStates.tf_find_transform('/palm_link','/obj_ar')
            P_.append(allegroStates.tf_pose_pub_process(initial_frame,'/obj','/obj_ar'))
            time.sleep(1)

            
            if(RECORD_BAG_):
                # recording bag file
                bag_name=bag_folder+objects.arr[index].name+'_'+str(t_no)+'_'+str(trial_no)
                bag_process=allegroStates.record_bag_process(bag_name)
                time.sleep(1)
        else:
            raw_input('Placed Object and Aligned initial frames?')

        # Run trajectory       
        joint_traj=np.loadtxt(open(traj_folder+objects.arr[index].f_name,'r'))
        

        allegroStates.publish_joint_trajectory(joint_traj)
        if(ARUCO_):
            suc=raw_input('Press Enter - store final pose \n Press F to store as failure ')


            if(suc==''):
                # Get object pose from aruco marker
                
                # Get final joint angles
                #joint_angles=allegroStates.joint_states.position
                # Get final object pose
                reached_pose=allegroStates.tf_find_transform('obj')
                objects.arr[index].reached=reached_pose
            else:
                # Store failure
                objects.arr[index].reached='F'

            if(RECORD_BAG_):
                bag_process.send_signal(subprocess.signal.SIGINT)
        
            objects.write_reached_pose(folder+trial_name)

            for i in P_:
                i.send_signal(subprocess.signal.SIGINT)
                #i.kill()

        next_=raw_input('Press Enter- to run another trial \nPress 1-to run different trajectory.\nPress 2- to exit program\n ')
        if(next_==''):
            repeat=0
        else:
            repeat=int(next_)
        
        
        

