# Copyright (C) 2017  Balakumar Sundaralingam, Tucker Hermans, University of Utah

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import pyOpt_wrapper_manipulator as pyOpt_wrapper

#import  manipulator_opt_functions as opt_fn
import  manipulator_opt_functions_time_parsing as opt_fn
class TrajOpt:
    '''
    This class contains trajectory optimization methods, currently has ILQR,
    SNOPT,SLSQP
    Initialization:
    x0= initial state
    x_des= desired state
    u= input to the system(shooting method)
    robot= robot model from robot_models.py
    control='SLSQP','SNOPT'
    '''
    def __init__(self,x0,x_des,u,robot,im_cost,fin_cost,grad,grad_fin,control,opt_options={}):
        self.x0=x0
        self.x_des=x_des
        self.u=u
        self.robot=robot
        self.control=control
        self.opt_options=opt_options
        self.opt_fns=opt_fn.OptFns(im_cost,fin_cost,grad,grad_fin,robot,x_des,x0)
        self.im_cost=im_cost
        self.fin_cost=fin_cost
    def optimize(self):
        if(self.control=='SLSQP'):
            controller=pyOpt_wrapper.OptimalController(self.robot,self.opt_fns,self.control,self.opt_options)
            u_opt,x_opt=controller.learn_controller(self.x0,self.x_des,self.u)
            return u_opt,x_opt

        elif(self.control=='SNOPT'):
            controller=pyOpt_wrapper.OptimalController(self.robot,self.opt_fns,self.control,self.opt_options)
            u_opt,x_opt=controller.learn_controller(self.x0,self.x_des,self.u)
            return u_opt,x_opt
        else:
            controller=pyOpt_wrapper.OptimalController(self.robot,self.opt_fns,self.control,self.opt_options)
            u_opt,x_opt=controller.learn_controller(self.x0,self.x_des,self.u)
            return u_opt,x_opt
            
