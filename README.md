# In-Grasp Manipulation using Relaxed-Rigidity constraints
This package performs trajectory optimization to obtain joint space trajectories for moving an object to a desired pose without changing the initial grasp. More information is available [here.](https://robot-learning.cs.utah.edu/project/in_hand_manipulation)
## Dependencies:
- [pyOpt](http://www.pyopt.org/)
- [ROS](http://www.ros.org/)
- [SNOPT](http://www.sbsi-sol-optimize.com/asp/sol_product_snopt.htm) (Can also use SLSQP by switching solver in pyOpt.)
##### Folders:
- scripts: contains all the python scripts used in our method, run main_tjo_relaxed_rigid.py
- data: contains all the goal poses,weights and reached poses.


## Generating intial data for planning:
We require initial joint angles of the grasp, the object pose with reference to the palm  and the desired pose for the object.

* Initial grasps are in data/initial_data/object_grasps
* Initial and the goal poses are in data/initial_data/goal_poses

## Generating Trajectories:
- main_tjo_relaxed_rigid.py
- main_tjo_relaxed_rigid_position.py
- main_tjo_ik_rigid.py
- main_tjo_pc.py- This is the point contact method refered to in the paper.

## Running trajectories:
- run_trajectory.py
- run_exp_method.py
